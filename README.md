## You'll want to make migrations when you change your models. Below are ##the different commands for you.

# Wardrobify

Team:

* Person 1 - Which microservice? Gabe Svetcos - Hats
Uses Location resources port 8090.


hats/api is a Django application with a Django project and a Django app already created. The Django app is not installed in the Django project. There are no URLs, views, or models yet created. You must create these.


hats/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file, hats/poll/poller.py, that you must implement to pull Location data from the Wardrobe API.


**The Hat resource should track its fabric, its style name, its color, a URL for a picture,
**and the location in the wardrobe where it exists.

-You must create RESTful APIs to get a list of hats, create a new hat, and delete a hat.
-You must create React component(s) to show a list of all hats and their details.
-You must create React component(s) to show a form to create a new hat.
-You must provide a way to delete a hat.
-You must route the existing navigation links to your components.


* Person 2 - Which microservice? Phat Shoes
Uses Bin resources port 8080.

shoes/api is a Django application with a Django project and a Django app already created. The Django app is not installed in the Django project. There are no URLs, views, or models yet created. You must create these.

shoes/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file, shoes/poll/poller.py, that you must implement to pull Bin data from the Wardrobe API.

*The Hat resource should track its fabric, its style name, its color, a URL for a picture,
*and the location in the wardrobe where it exists.

-You must create RESTful APIs to get a list of shoes, create a new shoe, and delete a shoe.
-You must create React component(s) to show a list of all shoes and their details.
-You must create React component(s) to show a form to create a new shoe.
-You must provide a way to delete a shoe.
-You must route the existing navigation links to your components.

## Model
-model name
-manufacturer
-color
-url




## Wardrobe API
The existing Wardrobe API can be accessed from your browser or Insomnia on port 8100.

The existing Wardrobe API can be accessed from your polling service on port 8000. .
Your service's poller will poll the base URL http://wardrobe-api:8000.

It has full RESTful APIs to interact with Bin and Location resources.


Method	        URL	                          What it does
  Get       /api/hats/                  Gets a list of all of the hats
  GET	      /api/bins/	                Gets a list of all of the bins
  GET	      /api/bins/<int:id>/	        Gets the details of one bin
  POST	    /api/bins/	                Creates a new bin with the posted data
  PUT	      /api/bins/<int:id>/	        Updates the details of one bin
  DELETE	  /api/bins/<int:id>/	        Deletes a single bin



Method	        URL	                            What it does
  GET	      /api/locations/	            Gets a list of all of the locations
  GET	      /api/locations/<int:id>/	  Gets the details of one location
  POST      /api/locations/	            Creates a new location with the posted data
  PUT	      /api/locations/<int:id>/	  Updates the details of one location
  DELETE	  /api/locations/<int:id>/	  Deletes a single location


## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

We created a shoe model that track its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe with the ability to.
-show a list of all shoes and their details.
-create a new shoe.
-delete a shoe.

We accessed the bins from the wardrobe by creating a poller that requests that data from the wardrobe api and we accessed that information via
BinV0 that we created.



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

We created a hat model that tracks its Style Name, Fabric, Color, and a Url for a picture.

It also has a location connected to it that shows the name of the closet, section, and shelf that it is in. This information is collected by polling the wardrobe API and then integrated into the Hat model using LocationVO model to import the data.

The current features allow you to see a list of hats, create new hats, and delete hats.
