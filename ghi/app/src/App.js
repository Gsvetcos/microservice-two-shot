import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatList from './HatList'
import HatForm from './HatForm';
import { useEffect, useState } from 'react';
import LocationForm from './LocationForm';

function App(props)
{
  const [hats, setHats] = useState([]);
  const [locations, setLocations] = useState([]);
  const [bins, setBins] = useState([]);
  const [shoes, setShoes] = useState([]);


  async function getHats()
  {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);
    if (response.ok)
    {
      const data = await response.json();
      setHats(data.hats);
    } else
    {
      console.error('An error occurred fetching the data')
    }

  }

  async function getLocations()
  {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok)
    {
      const data = await response.json();
      setLocations(data.locations);
    } else
    {
      console.error('An error occurred fetching the data')
    }

  }

  async function getShoes()
  {
    const url = 'http://localhost:8080/api/shoes/';
    const response = await fetch(url);
    if (response.ok)
    {
      const data = await response.json();
      setShoes(data.shoes);
    } else
    {
      console.error('An error occurred fetching the data')
    }

  }

  async function getBins()
  {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok)
    {
      const data = await response.json();
      setBins(data.bins);
    } else
    {
      console.error('An error occurred fetching the data')
    }

  }

  useEffect(() =>
  {
    getHats();
    getLocations();
    getBins();
    getShoes();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoeList />} />
          <Route path="shoes/new" element={<ShoeForm getShoes={getShoes}/>} />
          <Route path="hats">
            <Route index element={<HatList hats={hats} />} />
            <Route path="new" element={<HatForm getHats={getHats} />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm getLocations={getLocations} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
