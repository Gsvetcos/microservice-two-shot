import React, { useEffect, useState } from "react";
function ShoeForm({ getShoes }) {
  const [bins, setBins] = useState([]);
  const [bin, setBin] = useState("");
  const [modelName, setModelName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [color, setColor] = useState("");
  const [url, setUrl] = useState("");


  function handleModelNameChange(event) {
    const value = event.target.value;
    setModelName(value);
  }

  function handleManufacturerChange(event) {
    const value = event.target.value;
    setManufacturer(value);
  }

  function handleUrlChange(event) {
    const value = event.target.value;
    setUrl(value);
  }

  function handleColorChange(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleBinChange(event) {
    const value = event.target.value;
    setBin(value);
  }

  async function handleSubmit (event) {
    event.preventDefault();

    const data = {
      color,
      manufacturer,
      url,
      bin,
      model_name: modelName,
    };
    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    console.log(response)
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe)

      setColor("");
      setManufacturer("");
      setModelName("");
      setUrl("");
      setBin("");
      getShoes();
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins/"

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          {/* <form id="create-shoe-form"> */}
          <form onSubmit={handleSubmit} id="create-shoe-form">

            <div className="form-floating mb-3">
              <input
                onChange={handleModelNameChange}
                placeholder="Model Name"
                required
                type="text"
                name="modelName"
                id="modelName"
                className="form-control"
                value={modelName}
              />
              <label htmlFor="modelName">Model Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleManufacturerChange}
                placeholder="Manufacturer"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
                value={manufacturer}
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
                value={color}
              />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleUrlChange}
                placeholder="Url"
                required
                type="text"
                name="url"
                id="url"
                className="form-control"
                value={url}
              />
              <label htmlFor="url">Url</label>
            </div>

            <div className="mb-3">
              <select
                required
                name="bin"
                onChange={handleBinChange}
                id="bin"
                className="form-select"
                value={bin}
              >
                <option value="">Choose a bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.id} value={bin.id}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )





}

export default ShoeForm;
