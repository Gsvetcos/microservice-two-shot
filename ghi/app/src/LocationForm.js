import React, { useEffect, useState } from "react";

function LocationForm({ getLocations })
{
    const [closetName, setClosetName] = useState([]);
    const [sectionNumber, setSectionNumber] = useState("");
    const [shelfNumber, setShelfNumber] = useState("");

    function handleClosetNameChange(event)
    {
        const value = event.target.value;
        setClosetName(value);
    }

    function handleSectionNumberChange(event)
    {
        const value = event.target.value;
        setSectionNumber(value);
    }

    function handleShelfNumberChange(event)
    {
        const value = event.target.value;
        setShelfNumber(value);
    }

    async function handleSubmit(event)
    {
        event.preventDefault();
        const data = {
            closet_name: closetName,
            section_number: sectionNumber,
            shelf_number: shelfNumber,
        };
        const locationUrl = "http://localhost:8100/api/locations/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        console.log(response)
        if (response.ok)
        {
            const newLocation = await response.json();
            console.log(newLocation)

            setClosetName("");
            setSectionNumber("");
            setShelfNumber("");
            getLocations();
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new Hat</h1>
                    <form onSubmit={handleSubmit} id="add-hat-form">

                        <div className="form-floating mb-3">
                            <input
                                onChange={handleClosetNameChange}
                                placeholder="Closet Name"
                                required
                                type="text"
                                name="closetName"
                                id="closetName"
                                className="form-control"
                                value={closetName}
                            />
                            <label htmlFor="closetName">Closet Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                onChange={handleSectionNumberChange}
                                placeholder="sectionNumber"
                                required
                                type="text"
                                name="sectionNumber"
                                id="sectionNumber"
                                className="form-control"
                                value={sectionNumber}
                            />
                            <label htmlFor="sectionNumber">Section Number</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                onChange={handleShelfNumberChange}
                                placeholder="shelfNumber"
                                required
                                type="text"
                                name="shelfNumber"
                                id="shelfNumber"
                                className="form-control"
                                value={shelfNumber}
                            />
                            <label htmlFor="shelfNumber">Shelf Number</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )





}

export default LocationForm;
