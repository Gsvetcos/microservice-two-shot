import React, { useEffect, useState } from "react";

function HatForm({ getHats })
{
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState("");
    const [styleName, setStyleName] = useState("");
    const [fabric, setFabric] = useState("");
    const [color, setColor] = useState("");

    function handleStyleNameChange(event)
    {
        const value = event.target.value;
        setStyleName(value);
    }

    function handleFabricChange(event)
    {
        const value = event.target.value;
        setFabric(value);
    }

    function handleColorChange(event)
    {
        const value = event.target.value;
        setColor(value);
    }

    function handleLocationChange(event)
    {
        const value = event.target.value;
        setLocation(value);
    }

    async function handleSubmit(event)
    {
        event.preventDefault();
        const data = {
            color,
            fabric,
            style_name: styleName,
            location,
        };
        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        console.log(response)
        if (response.ok)
        {
            const newHat = await response.json();
            console.log(newHat)

            setColor("");
            setFabric("");
            setStyleName("");
            setLocation("");
            getHats()
        }
    };

    const fetchLocations = async () =>
    {
        const url = "http://localhost:8100/api/locations/"

        const response = await fetch(url);

        if (response.ok)
        {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    useEffect(() =>
    {
        fetchLocations();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Hat</h1>
                    <form onSubmit={handleSubmit} id="add-hat-form">

                        <div className="form-floating mb-3">
                            <input
                                onChange={handleStyleNameChange}
                                placeholder="Style Name"
                                required
                                type="text"
                                name="styleName"
                                id="styleName"
                                className="form-control"
                                value={styleName}
                            />
                            <label htmlFor="styleName">Style Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                onChange={handleFabricChange}
                                placeholder="Fabric"
                                required
                                type="text"
                                name="fabric"
                                id="fabric"
                                className="form-control"
                                value={fabric}
                            />
                            <label htmlFor="fabric">Fabric</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input
                                onChange={handleColorChange}
                                placeholder="Color"
                                required
                                type="text"
                                name="color"
                                id="color"
                                className="form-control"
                                value={color}
                            />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="mb-3">
                            <select
                                required
                                name="location"
                                onChange={handleLocationChange}
                                id="location"
                                className="form-select"
                                value={location}
                            >
                                <option value="">Choose a location</option>
                                {locations.map((location) =>
                                {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )





}

export default HatForm;
