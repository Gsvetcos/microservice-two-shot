import React, { useEffect, useState } from "react";

function ShoeList() {

  const [shoes, setShoes] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/"

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };

  const handleDeleteShoe = async (id) => {
    const shoeUrl = `http://localhost:8080/api/shoes/${id}`;
    const response = await fetch(
      shoeUrl,
      { method: "delete" }
    );
    if (response.ok) {
      shoes.filter((shoe) => shoe.id !== id);
      window.location.replace("/shoes")
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Url</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td>{shoe.url}</td>
                <td>
                  <button className="btn btn-danger" onClick={() => handleDeleteShoe(shoe.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <img src="push.gif" alt="Push the button" />
    </>
  );
}

export default ShoeList;
