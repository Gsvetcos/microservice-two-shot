import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function HatColumn(props)
{
    const handleDelete = async (hatId) =>
    {
        try
        {
            const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
                method: "DELETE",
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            console.log(response)
            if (response.ok)
            {
                const data = await response.json();
                // success message(?)
                console.log(data)
            } else
            {
                console.error('Error deleting hat:', response.status);
            }
        } catch (error)
        {
            console.error('Network Error', error);
        }
    }

    return (
        <div className="col">
            {props.list.map(hat =>
            {
                return (
                    <div key={hat.href} className="card mb-3 shadow">
                        <img src={hat.picture_url} className="card-img-top" alt="" />
                        <div className="card-body">
                            <h5 className="card-title">{hat.fabric} - {hat.style_name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted">
                                {hat.color}
                            </h6>
                            <p className="card-text">
                                {hat.location.closet_name}
                            </p>
                        </div>
                        <div className="card-footer d-grid gap-2 d-md-flex justify-content-md-end">
                            <button onClick={() => handleDelete(hat.id)} type="button" className="btn btn-danger btn-sm">Delete</button>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

function HatList({ hats })
{
    const [hatColumns, setHatColumns] = useState([]);

    useEffect(() =>
    {
        async function getHatDetails()
        {
            try
            {
                // Create a list of for all the requests and
                // add all of the requests to it
                const requests = [];
                for (let hat of hats)
                {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                // Wait for all of the requests to finish
                // simultaneously
                const responses = await Promise.all(requests);

                // Set up the "columns" to put the conference
                // information into
                const hatColumns = [[], [], []];

                // Loop over the conference detail responses and add
                // each to to the proper "column" if the response is
                // ok
                let i = 0;
                for (const hatResponse of responses)
                {
                    if (hatResponse.ok)
                    {
                        const details = await hatResponse.json();
                        hatColumns[i].push(details);
                        i = i + 1;
                        if (i > 2)
                        {
                            i = 0;
                        }
                    } else
                    {
                        console.error(hatResponse);
                    }
                }

                // Set the state to the new list of three lists of
                // conferences
                setHatColumns(hatColumns);

            } catch (e)
            {
                console.error(e);
            }
        }
        getHatDetails();
    }, [hats])

    return (
        <>
            <div className="px-4 py-5 my-5 mt-0 text-center">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">Wardrobify</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                        Always know where you hang your hat.
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a hat</Link>
                    </div>
                </div>
            </div>
            <div className="container">
                <h2>My Hats</h2>
                <div className="row">
                    {hatColumns.map((hatList, index) =>
                    {
                        return (
                            <HatColumn key={index} list={hatList} />
                        );
                    })}
                </div>
            </div>
        </>
    );
}

export default HatList;
