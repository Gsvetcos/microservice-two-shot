import django
import os
import sys
import time
import json
import requests

sys.path.append("")
# sys.path.append("shoes")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinV0
# Import models from hats_rest, here.
# from shoes_rest.models import Something

def get_all_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    for bin in content["bins"]:
        BinV0.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "closet_name": bin["closet_name"],
                "bin_number": bin["bin_number"],
                "bin_size": bin["bin_size"],
            },
        )



def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_all_bins()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(15)


if __name__ == "__main__":
    poll()
