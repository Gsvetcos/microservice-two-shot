from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import BinV0, Shoe
from common.json import ModelEncoder

class BinV0Encoder(ModelEncoder):
    model = BinV0
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "id",
        "bin",
        "url",
    ]
    encoders = {
        "bin": BinV0Encoder()
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
        "bin",
        "url",
    ]
    encoders = {
        "bin": BinV0Encoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            # href = f'/api/bins/{content["bin"]}/'
            # bin = BinV0.objects.get(import_href=href)
            # content["bin"] = bin
            bin_id = content["bin"]
            bin = BinV0.objects.get(id=bin_id)
            content["bin"] = bin
        except BinV0.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.filter(id=id)
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoeListEncoder,
            )
    else:
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
