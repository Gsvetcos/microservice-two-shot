from django.db import models

# Create your models here.


class BinV0(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(
        max_length=200,
        unique=True,
        null=True,
    )

    def __str__(self):
        return self.closet_name

    class Meta:
        ordering = (
            "closet_name",
            "bin_number",
            "bin_size",
            "import_href"
            )


class Shoe(models.Model):
    model_name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinV0,
        on_delete=models.CASCADE,
        related_name='shoes',
        null=True,
    )

    def __str__(self):
        return self.model_name

    class Meta:
        ordering = (
            "model_name",
            "manufacturer",
            "color",
            "url",
            )
